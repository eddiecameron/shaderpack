// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/VCRWarp" {
	Properties {
		_MainTex ("Base", 2D) = "" {}
		_WarpScale( "Warp scale", float ) = 0.5
		_WarpMap( "Warp map(alpha)", 2D) = "black" {}
	}
	
	CGINCLUDE
	
	#include "UnityCG.cginc"
	
	struct v2f {
		float4 pos : POSITION;
		float2 uv : TEXCOORD0;
	};
	
	sampler2D _MainTex;
	
	float4 _MainTex_TexelSize;
	float _WarpScale;
	float _WarpOffset;
	sampler2D _WarpMap;

		
	v2f vert( appdata_img v ) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		
		#if SHADER_API_D3D9 || SHADER_API_D3D11 || SHADER_API_XBOX360
		if (_MainTex_TexelSize.y < 0)
			 v.texcoord.y = 1.0 - v.texcoord.y ;
		#endif
		
		o.uv = v.texcoord.xy;
		return o;
	} 
	
	half4 fragSimpleCopy(v2f i) : COLOR {
		return tex2D (_MainTex, i.uv.xy);
	}

	half4 frag(v2f i) : COLOR {
		half2 uv = i.uv;
		half2 warpUV = uv;
		warpUV.y += _WarpOffset;
		uv.x += ( tex2D(_WarpMap, warpUV ).a - 0.5 ) * _WarpScale * _MainTex_TexelSize;
		
		half4 color = tex2D (_MainTex, uv);
		
		return color;
	}

	ENDCG 
	
Subshader {
// Pass {
//	  ZTest Always Cull Off ZWrite Off
//	  Fog { Mode off }      
//
//      CGPROGRAM
//      
//      #pragma vertex vert
//      #pragma fragment fragSimpleCopy
//      
//      ENDCG
//  }
Pass {
	  ZTest Always Cull Off ZWrite Off
	  Fog { Mode off }      

      CGPROGRAM
      
      #pragma vertex vert
      #pragma fragment frag
      
      ENDCG
  }
}

Fallback off
	
} // shader