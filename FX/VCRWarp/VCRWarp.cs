using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VCRWarp : MonoBehaviour 
{
	public Texture vcrWarpMap;
    public Shader vcrWarpShader;
    public float moveWarpTime = 15f;
    public float warpScale = 5f;

	Material vcrWarpMat;
	float warpOffset = 0;
	
    void OnEnable(){
        GetComponent<Camera>().depthTextureMode = DepthTextureMode.DepthNormals;
    }

	void OnDisable()
	{
		if ( vcrWarpMat )
			DestroyImmediate ( vcrWarpMat );
		
		StopAllCoroutines();
	}
		
	void OnRenderImage( RenderTexture source, RenderTexture dest )
	{
		if ( !vcrWarpMat )
		{
			vcrWarpMat = new Material(vcrWarpShader );
			vcrWarpMat.SetTexture ( "_WarpMap", vcrWarpMap );
        }

        vcrWarpMat.SetFloat ( "_WarpScale", warpScale );
		warpOffset += Time.deltaTime / moveWarpTime / 2f;
		vcrWarpMat.SetFloat ( "_WarpOffset", warpOffset );
		Graphics.Blit ( source, dest, vcrWarpMat );
	}
}
