Shader "WorldUV/Specular" 
{

	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular colour", Color ) = (0,0,0,0)
		_Shininess ("Shininess", Range(0,1) ) = 0
		_MainTex ("Wall Front Texture (RGB)", 2D) = "surface" {} 
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }

		CGPROGRAM
		#include "UnityCG.cginc"
		
		#pragma surface surf BlinnPhong

		struct Input 
		{
			float3 worldPos;
			float3 worldNormal;
		};

		sampler2D _MainTex;
		half4 _MainTex_ST;
		float4 _Color;
		half _Shininess;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 UV;
		    
			if( abs(IN.worldNormal.x)>0.5) 
			{
				UV = IN.worldPos.zy; // side
			}
			else if(abs(IN.worldNormal.z)>0.5) 
			{
				UV = IN.worldPos.xy; // front
			} 
			else 
			{
				UV = IN.worldPos.xz; // top
			}
			
			fixed4 tex = tex2D( _MainTex, TRANSFORM_TEX( UV, _MainTex ) );
		
			o.Albedo = tex.rgb * _Color.rgb;
			o.Gloss = tex.a;
			o.Alpha = tex.a * _Color.a;
			o.Specular = _Shininess;
		}

		ENDCG
	} 

	Fallback "VertexLit"
}