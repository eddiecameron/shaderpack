// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "WorldUV/Bumped Specular" 
{

	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Colour", Color) = (1,1,1,1)
		_Shininess("Shininess", Range(0,1) ) = 1
		_MainTex ("Main Texture (RGB)", 2D) = "surface" {} 
      	_BumpMap ("Bumpmap", 2D) = "bump" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }

		CGPROGRAM
		#include "UnityCG.cginc"
		
		#pragma surface surf BlinnPhong vertex:vert

		struct Input 
		{
			float2 worldUV;
		};

		sampler2D _MainTex;
      	sampler2D _BumpMap;
		half4 _MainTex_ST;
		float4 _Color;
		half _Shininess;
		
		void vert( inout appdata_full v, out Input o )
		{   
			float2 UV;
		    float3 normal = mul( unity_ObjectToWorld, float4( v.normal, 0 ) ).xyz;
		    float3 worldPos = mul (unity_ObjectToWorld, v.vertex).xyz;
		    
			if( abs( normal.x ) > 0.5) 
			{
				UV = worldPos.zy; // side
			}
			else if(abs( normal.z)>0.5) 
			{
				UV = worldPos.xy; // front
			} 
			else 
			{
				UV = worldPos.xz; // top
			}
						       
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.worldUV = TRANSFORM_TEX( UV, _MainTex );
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 tex = tex2D( _MainTex, IN.worldUV );
			
			o.Albedo = tex.rgb * _Color.rgb;
        	o.Normal = UnpackNormal (tex2D (_BumpMap, IN.worldUV ));
			o.Gloss = tex.a;
			o.Alpha = tex.a * _Color.a;
			o.Specular = _Shininess;
		}

		ENDCG
	} 

	Fallback "VertexLit"
}