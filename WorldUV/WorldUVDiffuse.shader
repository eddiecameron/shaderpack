Shader "WorldUV/Diffuse" 
{

	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Wall Front Texture (RGB)", 2D) = "surface" {} 
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }

		CGPROGRAM
		#include "UnityCG.cginc"
		
		#pragma surface surf Lambert

		struct Input 
		{
			float3 worldPos;
			float3 worldNormal;
		};

		sampler2D _MainTex;
		half4 _MainTex_ST;
		float4 _Color;
		float _Scale;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 UV;
		    
			if( abs(IN.worldNormal.x)>0.5) 
			{
				UV = IN.worldPos.yz; // side
			}
			else if(abs(IN.worldNormal.z)>0.5) 
			{
				UV = IN.worldPos.xy; // front
			} 
			else 
			{
				UV = IN.worldPos.xz; // top
			}
			
			o.Albedo = tex2D( _MainTex, TRANSFORM_TEX( UV, _MainTex ) ).rgb * _Color;
		}

		ENDCG
	} 

	Fallback "VertexLit"
}