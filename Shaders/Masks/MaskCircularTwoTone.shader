﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Unlit circular two-tone shader, masked
// Point with UV angle greater than threshold will be colour2, otherwise colour1
// Used for circular progress bars and the like

Shader "Mask/UnlitCircluarTwoTone" 
{
Properties {
	_MainTex ("Mask (A)", 2D ) = "white" {}
	_Color ("Colour 1", color) = ( 1, 1, 1, 1 )
	_Color2 ( "Colour 2", color ) = (1, 1, 1, 1 )
	_Threshold ( "Threshold angle", Range( 0, 7 ) ) = 3
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 100
	
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha 

	Pass 
	{
		Lighting Off
		
		CGPROGRAM
		#include "UnityCG.cginc"
		
		#pragma vertex vert
		#pragma fragment frag
	
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float4 _Color;
		uniform float4 _Color2;
		uniform half _Threshold;
		
		struct v2f
		{
			float4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
		};
		
		v2f vert( appdata_img v )
		{
			v2f o;
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			
			return o;
		}

		float4 frag(v2f i) : COLOR 
		{
			half angle = atan2( i.uv.y - 0.5, i.uv.x - 0.5 ) + 3.14159;
			
			float4 color;
			if ( angle > _Threshold )
				color = _Color2;
			else
				color = _Color;
				
			color.a *= tex2D( _MainTex, i.uv ).a;
			return color;
		}
		
		ENDCG
	}
}
}
