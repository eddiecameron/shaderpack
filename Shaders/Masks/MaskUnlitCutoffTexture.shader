﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Unlit alpha-blended shader.
// - no lighting
// - no lightmap support

Shader "Mask/Unlit Cutoff Texture" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_MaskTex ("Mask (A)", 2D ) = "white" {}
	_Color ("Colour", color) = (1,1,1,1)
    _Cutoff ( "Cutoff Alpha", Range( 0, 1 ) ) = .5
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 100
	
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha 

	Pass 
	{
		Lighting Off
		
		CGPROGRAM
		#include "UnityCG.cginc"
		
		#pragma vertex vert
		#pragma fragment frag
	
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform sampler2D _MaskTex;
		uniform float4 _MaskTex_ST;
		uniform float4 _Color;
		uniform float _Cutoff;

		struct v2f
		{
			float4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
			half2 maskUv : TEXCOORD1;
		};
		
		v2f vert( appdata_img v )
		{
			v2f o;
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv = TRANSFORM_TEX( v.texcoord, _MainTex);
			o.maskUv = TRANSFORM_TEX( v.texcoord, _MaskTex);
			
			return o;
		}

		float4 frag(v2f i) : COLOR 
		{
			float4 color = _Color;
			color.a *= tex2D( _MaskTex, i.maskUv ).a > _Cutoff;
			return tex2D(_MainTex, i.uv) * color;
		}
		
		ENDCG
	}
}
}
