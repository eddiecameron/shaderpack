﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "ScreenSpaceTexture/Unlit" {
Properties {
	_Color ("Color", Color) = (1, 1, 1, 1)
	_MainTex ("Particle Texture", 2D) = "white" {}
}

Category {
	Tags { "Queue"="Geometry"}
	Lighting Off
	
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
            fixed4 _Color;

			struct appdata_t {
				float4 vertex : POSITION;
			};

			struct v2f {
				float4 vertex : POSITION;
				float4 scrPos : TEXCOORD1;
			};
			
			float4 _MainTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.scrPos = ComputeScreenPos (o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{				
                float2 windowPos = ( i.scrPos.xy / i.scrPos.w ).xy;
				return tex2D( _MainTex, windowPos ) * _Color;
			}
			ENDCG 
		}
	}
}
}
