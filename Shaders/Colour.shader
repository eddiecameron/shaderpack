// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Unlit Colour - Just set a colour and that's it
Shader "Unlit/ColouredTexture" 
{
	Properties 
	{
		_MainTex( "Main Texture", 2D ) = "white" {}
		_Color ("Colour", Color) = ( 0.5, 0.5, 0.5, 1 )
	}
	
	SubShader 
	{
      Tags { "Queue"="Geometry" "RenderType"="Opaque" }

      Pass {
         Cull Off
         Lighting Off

         CGPROGRAM
         #pragma vertex vert
         #pragma fragment frag
         #include "UnityCG.cginc"

         struct vertexInput {
            float4 vertex : POSITION;
            float2 texcoord : TEXCOORD0;
         };

         struct vertexOutput {
            float4 vertex : SV_POSITION;
            float2 texcoord : TEXCOORD0;
         };

         sampler2D _MainTex;
         float4 _MainTex_ST;

         vertexOutput vert(vertexInput input)
         {
            vertexOutput output;
            output.vertex = UnityObjectToClipPos(input.vertex);
            output.texcoord = TRANSFORM_TEX(input.texcoord,_MainTex);
            return output;
         }

         half4 _Color;

         fixed4 frag (vertexOutput input) : COLOR
         {
         	return tex2D( _MainTex, input.texcoord ) * _Color;
         }
         ENDCG 
      }
	}
}
