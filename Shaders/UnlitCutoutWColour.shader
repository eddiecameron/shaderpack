// Unlit alpha-cutout shader w/ colour override
// - no lighting
// - no lightmap support

Shader "Unlit/Transparent Cutout(with colour)" 
{
	Properties 
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_Color ( "Colour", Color ) = (0.5, 0.5, 0.5, 1 )
	}

	SubShader 
	{
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
		LOD 100
		
		Pass 
		{
			Lighting Off
			Alphatest Greater [_Cutoff]
			SetTexture[_MainTex]
			{
				constantColor [_Color]
				combine constant, texture
			}
		}
	}
}