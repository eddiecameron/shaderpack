/* FullScreenColour.cs
 * Copyright Eddie Cameron & Grasshopper 2013
 * ----------------------------
 * Implements the Full Screen Colour shader
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[AddComponentMenu( "Image Effects/Full Screen Colour" )]

public class FullScreenColour : MonoBehaviour 
{
    public Shader fullScreenColourShader;

    Material fullScreenColourMat;

    Color lastColour;
    public Color screenColour = Color.clear;

	protected void OnEnable()
    {
        if ( fullScreenColourShader == null )
            fullScreenColourShader = Shader.Find ( "Image Effects/Full Screen Colour" );

        if ( fullScreenColourShader == null || !fullScreenColourShader.isSupported )
        {
            Debug.LogWarning( "Full Screen Shader not provided or not supported" );
            enabled = false;
        }
        else
        {
            fullScreenColourMat = new Material( fullScreenColourShader );
        }
	}

	protected void Start()
	{
	}

	protected void Update()
	{
	}

    void OnRenderImage( RenderTexture source, RenderTexture dest )
    {
        if ( enabled && fullScreenColourMat )
        {
            if ( screenColour != lastColour )
            {
                fullScreenColourMat.color = screenColour;
                lastColour = screenColour;
            }

            Graphics.Blit( source, dest, fullScreenColourMat );
        }
    }
}
