// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Image Effects/Full Screen Colour" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,0)
		_MainTex( "Screen tex", 2D) = "white" {}
	}

		
	CGINCLUDE
	#include "UnityCG.cginc"
	
	fixed4 _Color;
	sampler2D _MainTex;
	
	struct v2f 
	{
		float4 pos : POSITION;
		half2 uv : TEXCOORD0;
	};
		
	v2f vert( appdata_img v )
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy;
		return o;
	}
		
	
	float4 frag ( v2f i ) : COLOR
	{
		half colourAlpha = _Color.a;
		fixed4 screenColour = tex2D( _MainTex, i.uv ) * ( 1 - _Color.a );
		fixed4 filterColour = _Color * _Color.a;
		
		fixed4 returnColour = screenColour + filterColour;
		returnColour.a = 1;
	
		return returnColour;
	}
	ENDCG
		
	SubShader 
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off
			Fog { Mode off }      
			//	ZWrite On
			//	Color[_Color]
			//	Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}
}
